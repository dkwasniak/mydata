package dkdev.com.mydata;


import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {



    protected static Context context;

    private static AppPreferences appPreferences;

    protected static SharedPreferences preferences;

    private static final String PREFERENCES_FILE = "shared_file";

    private static final String USER_COOKIE = "user_cookie";


    public AppPreferences(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

    }

    public static synchronized void initializeInstance(Context context) {
        if(appPreferences == null) {
            appPreferences = new AppPreferences(context);
        }
    }

    public static synchronized AppPreferences getInstance() {
        if (appPreferences == null) {
            throw new IllegalStateException(AppPreferences.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return appPreferences;
    }

    public void setUserCookie(String cookie) {
        preferences.edit().putString(USER_COOKIE, cookie).apply();
    }

    public String getCookies() {
        return preferences.getString(USER_COOKIE,"");
    }

}
