package dkdev.com.mydata;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import roboguice.RoboGuice;

/**
 * Created by Damian Kwasniak on 30.05.15.
 */
public class MyDataApplication extends Application {

	public static final String TAG = MyDataApplication.class.getSimpleName();

	private static MyDataApplication instance;

	private RequestQueue requestQueue;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		AppPreferences.initializeInstance(this);
	}
	static {
		RoboGuice.setUseAnnotationDatabases(false);
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public static synchronized MyDataApplication getInstance() {
		return instance;
	}

	public RequestQueue getRequestQueue() {
		if (requestQueue == null) {
			requestQueue = Volley.newRequestQueue(getApplicationContext());
		}
		return requestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (requestQueue != null) {
			requestQueue.cancelAll(tag);
		}
	}
}
