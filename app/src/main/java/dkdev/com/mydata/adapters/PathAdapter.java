package dkdev.com.mydata.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dkdev.com.mydata.R;
import dkdev.com.mydata.api.models.Directory;

import static dkdev.com.mydata.adapters.PathAdapter.ViewHolder.*;


public class PathAdapter extends RecyclerView.Adapter<PathAdapter.ViewHolder> {

    public interface PathAdapterCallback {
        void onPathItemClicked(int id);
    }

    private Directory directory;


    private PathAdapterCallback callback;

    public PathAdapter(Directory directory, PathAdapterCallback callback) {
        this.directory = directory;
        this.callback = callback;
    }

    public PathAdapter() {

    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView item;
        private PathAdapterCallback callback;
        private Directory directory;
        public ViewHolder(View itemView, Directory directory, PathAdapterCallback callback) {
            super(itemView);
            item = (TextView) itemView.findViewById(R.id.itemTextView);
            this.directory = directory;
            this.callback = callback;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int pos = getLayoutPosition();
            if(callback != null) {
                callback.onPathItemClicked(directory.getPathToDirectory().get(pos).getId());
            }
        }

    }

    @Override
    public PathAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.path_row, parent, false);
        return new ViewHolder(itemView, directory, callback);
    }

    @Override
    public void onBindViewHolder(PathAdapter.ViewHolder holder, int position) {
        String name = directory.getPathToDirectoryAsList().get(position);
        holder.item.setText(name);
    }

    @Override
    public int getItemCount() {
        if (directory != null) {
            return directory.getPathToDirectoryAsList().size();
        }
        return 0;
    }

}
