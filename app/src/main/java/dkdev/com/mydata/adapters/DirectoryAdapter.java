package dkdev.com.mydata.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import dkdev.com.mydata.R;
import dkdev.com.mydata.api.models.Directory;
import dkdev.com.mydata.api.models.File;
import dkdev.com.mydata.api.models.SubDirectory;

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.ViewHolder> {


    public static final int DIRECTORY = 0;

    public static final int FILE = 1;

    public interface DirectoryAdapterCallback {

        void onItemClick(Directory directory);

        void onFileClicked(File file);

        void onSubDirectoryClicked(SubDirectory subDirectory);

        void onFileLongClick(File file);

        void onSubDirectoryLongClick(SubDirectory subDirectory);

    }

    private Directory directory;

    private DirectoryAdapterCallback callback;

    public DirectoryAdapter(Directory directory, DirectoryAdapterCallback callback) {
        this.directory = directory;
        this.callback = callback;
    }

    public DirectoryAdapter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.data_row, parent, false);

        return new ViewHolder(itemView, callback);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (position > directory.getSubDirectories().size() - 1) {
            File file = directory.getFiles().get(position - directory.getSubDirectories().size());
            holder.image.setImageResource(R.drawable.file_icon);
            holder.directoryNameTextView.setText(file.getName());
            holder.setType(FILE);
            holder.setFile(file);
        } else {
            SubDirectory subDirectory = directory.getSubDirectories().get(position);
            holder.directoryNameTextView.setText(subDirectory.getName());
            holder.setType(DIRECTORY);
            holder.setSubDirectory(subDirectory);
        }

    }

    @Override
    public int getItemCount() {
        if(directory != null) {
            return directory.getSubDirectories().size() + directory.getFiles().size();
        }
        return 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {


        TextView directoryNameTextView;
        ImageView image;
        DirectoryAdapterCallback callback;
        File file;
        SubDirectory subDirectory;
        private int type;

        public ViewHolder(View itemView, DirectoryAdapterCallback callback) {
            super(itemView);
            directoryNameTextView = (TextView) itemView.findViewById(R.id.directoryNameTextView);
            image = (ImageView) itemView.findViewById(R.id.iconImageView);
            this.callback = callback;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setSubDirectory(SubDirectory subDirectory) {
            this.subDirectory = subDirectory;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public void setType(int type) {
            this.type = type;
        }

        @Override
        public void onClick(View v) {
            int a = getLayoutPosition();
            if(callback != null) {
               if(type == DIRECTORY) {
                   callback.onSubDirectoryClicked(subDirectory);
               } else {
                   callback.onFileClicked(file);
               }
            }
        }


        @Override
        public boolean onLongClick(View v) {
            if(type == DIRECTORY) {
                callback.onSubDirectoryLongClick(subDirectory);
            } else {
                callback.onFileLongClick(file);
            }
            return false;
        }
    }
}
