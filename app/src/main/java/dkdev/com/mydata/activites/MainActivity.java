package dkdev.com.mydata.activites;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.loopj.android.http.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import dkdev.com.mydata.AppPreferences;
import dkdev.com.mydata.R;
import dkdev.com.mydata.api.ApiConstants;
import dkdev.com.mydata.api.ApiData;
import dkdev.com.mydata.api.ApiError;
import dkdev.com.mydata.api.ApiParser;
import dkdev.com.mydata.api.ApiRequest;
import dkdev.com.mydata.api.ApiResponse;
import dkdev.com.mydata.api.enumeration.EHttp;
import dkdev.com.mydata.api.enumeration.ERequest;
import dkdev.com.mydata.api.enumeration.ResponseType;
import dkdev.com.mydata.api.models.Directory;
import dkdev.com.mydata.api.models.File;
import dkdev.com.mydata.api.models.SubDirectory;
import dkdev.com.mydata.base.BaseActivity;
import dkdev.com.mydata.base.BaseFragment;
import dkdev.com.mydata.base.BaseFragment.BaseFragmentListener;
import dkdev.com.mydata.base.FragmentType;
import dkdev.com.mydata.dialogs.AddDirOrFileSelectDialog;
import dkdev.com.mydata.dialogs.AddDirectoryDialog;
import dkdev.com.mydata.dialogs.AddFileDialog;
import dkdev.com.mydata.dialogs.AddUserDialog.AddUserDialogCallback;
import dkdev.com.mydata.fragments.MainFragment;
import dkdev.com.mydata.fragments.ShowImageFragment;
import dkdev.com.mydata.login.LoginActivity;
import dkdev.com.mydata.utils.GlobalUtils;

import static dkdev.com.mydata.base.FragmentType.MAIN_FRAGMENT;
import static dkdev.com.mydata.base.FragmentType.SHOW_VIDEO_FRAGMENT;
import static dkdev.com.mydata.base.FragmentType.SHOW_IMAGE_FRAGMENT;
import static java.util.Arrays.asList;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, BaseFragmentListener,
        ApiRequest.OnNetworkListener, AddDirectoryDialog.AddDirectoryDialogCallback,
        MainFragment.MainFragmentCallback, AddFileDialog.AddFileDialogCallback, AddUserDialogCallback {

    private static final String BASE_FRAGMENT_TAG = "base_fragment_type";
    private FragmentType currentFragmentType = MAIN_FRAGMENT;

    private BaseFragment currentFragment;

    private Directory currentDirectory;

    private int backPressCounter;

    private ProgressDialog progressDialog;

    private FloatingActionButton fab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDirectoryDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getDirectoryFromApi(1);
        setMainFragment();
        navigationView.getMenu().getItem(0).setChecked(true);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (currentFragment.getFragmentType().equals(MAIN_FRAGMENT)) {
                if (currentDirectory.getId() > 1) {
                    getDirectoryFromApi(currentDirectory.getPathToDirectory().get
                            (currentDirectory.getPathToDirectory().size()-1).getId());
                } else {
                    backPressCounter++;
                    if (backPressCounter == 2) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                backPressCounter = 0;
                            }
                        }, 1000);
                        Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (currentFragment.getFragmentType().equals(SHOW_VIDEO_FRAGMENT) || currentFragment.getFragmentType().equals(SHOW_IMAGE_FRAGMENT)) {
                getDirectoryFromApi(currentDirectory.getId());
                setMainFragment();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
        } else if (id == R.id.nav_manage) {
            showLogoutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setMainFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        BaseFragment fragment = MainFragment.newInstance(currentDirectory);
        ft.replace(R.id.main_container, fragment, BASE_FRAGMENT_TAG).addToBackStack(BASE_FRAGMENT_TAG)
                .commit();
        currentFragmentType = fragment.getFragmentType();
        currentFragment = fragment;
        fab.setVisibility(View.VISIBLE);
    }

    private void showFragment(BaseFragment fragment) {

    }

    private FragmentType getCurrentFragmentType() {
        return currentFragmentType;
    }

    @Override
    public void onChangeFragment(FragmentType type) {

    }

    @Override
    public void onChangeFragment(BaseFragment fragment) {
        if (fragment == null) {
            throw new NullPointerException("Fragment can not be null");
        }

        if (!getCurrentFragmentType().equals(fragment.getFragmentType())) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_container, fragment, BASE_FRAGMENT_TAG).addToBackStack(BASE_FRAGMENT_TAG).commit();
            currentFragmentType = fragment.getFragmentType();
            currentFragment = fragment;
            if (fragment.getFragmentType() != FragmentType.MAIN_FRAGMENT) {
                fab.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void showLogoutDialog() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle("Wyloguj");
        builder.setMessage("Jesteś pewny ?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });
        builder.setNegativeButton("Anuluj", null);
        builder.show();
    }

    private void logout() {
        new ApiRequest(this,
                new ApiData(ApiConstants.getUserLogout(), null, EHttp
                        .GET, ERequest.USER_LOGOUT_REQUEST), ResponseType.OBJECT, this).execute();
    }

    private void showAddDirectoryDialog() {
        new AddDirOrFileSelectDialog().show(getSupportFragmentManager(), "");
    }

    @Override
    public void onRequestSuccess(ApiResponse response) {
        switch (response.getRequestType()) {
            case USER_LOGOUT_REQUEST:
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                AppPreferences.getInstance().setUserCookie("");
                break;
            case GET_DIRECTORY:
                currentDirectory = ApiParser.getDirectoryFromResponse(response.getResponse());
                reloadFragmentView();
                break;
            case ADD_DIRECTORY:
                getDirectoryFromApi(currentDirectory.getId());
                break;
            case DELETE_FILE:
                getDirectoryFromApi(currentDirectory.getId());
                break;
            case DELETE_DIRECTORY:
                getDirectoryFromApi(currentDirectory.getId());
                break;
        }
    }

    @Override
    public void onRequestFailed(ApiError error) {
        if (error.getCode() == 401) {
            Toast.makeText(this, R.string.login_again, Toast.LENGTH_LONG).show();
            AppPreferences.getInstance().setUserCookie("");
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            Toast.makeText(this, error.getErrorMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void reloadFragmentView() {
        if (currentFragment instanceof MainFragment) {
            ((MainFragment) currentFragment).reloadAdapters(currentDirectory);
        }
    }

    private void getDirectoryFromApi(int directoryId) {
        new ApiRequest(this,
                new ApiData(ApiConstants.getDirectory() + String.valueOf(directoryId), null, EHttp
                        .GET, ERequest.GET_DIRECTORY), ResponseType.OBJECT, this).execute();

    }

    private void addNewDirectory(String name, boolean readOnly, boolean priv) {
        Map<String, String> params = GlobalUtils.getRequestParams(
                asList("name", "parentId", "readOnly","type"),
                asList(name, currentDirectory.getId().toString(), readOnly ? "true" : "false", priv ? "Prywatny" : "Publiczny"));
        new ApiRequest(this,
                new ApiData(ApiConstants.getAddDirectory(), params, EHttp
                        .POST, ERequest.ADD_DIRECTORY), ResponseType.OBJECT, this).execute();

    }

    @Override
    public void onSendAddDirectoryRequest(String name, boolean readOnly,boolean priv) {
        addNewDirectory(name, readOnly, priv);
    }

    @Override
    public void onDirectoryClicked(int id) {
        getDirectoryFromApi(id);
    }

    @Override
    public void onFileClicked(File file) {
        Bundle bundle = new Bundle();
        bundle.putString("fileName", file.getName());
        bundle.putString("fileId", file.getId().toString());
        bundle.putString("directoryId", currentDirectory.getId().toString());
        if (file.getMimeType().contains("image") || file.getMimeType().contains("jpg") ||
                file.getMimeType().contains("jpeg") || file.getMimeType().contains("gif") ||
                file.getMimeType().contains("png") || file.getMimeType().contains("bmp")) {
            BaseFragment fragment = ShowImageFragment.newInstance(currentDirectory, file);
            onChangeFragment(fragment);
        } else if (file.getMimeType().contains("video")) {
            Intent intent = new Intent(this, DownloadManagerActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            downloadFile(file);
        }
    }

    @Override
    public void onFileLongClick(File file) {
        showOnDeleteFileDialog(file);
    }

    @Override
    public void onDirectoryLongClick(SubDirectory subDirectory) {
        showOnDeleteDirectoryDialog(subDirectory);
    }


    private void showOnDeleteFileDialog(final File file) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle("Usun plik");
        builder.setMessage("Jesteś pewny? Tej akcji nie można cofnąć!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFileRequest(file);
            }
        });
        builder.setNegativeButton("Anuluj", null);
        builder.show();
    }

    private void deleteFileRequest(File file) {
        new ApiRequest(this,
                new ApiData(ApiConstants.getDirectory()+"/"+currentDirectory.getId()+"/file/"
                        +file.getId(), null, EHttp
                        .DELETE, ERequest.DELETE_FILE), ResponseType.OBJECT, this).execute();
    }

    private void showOnDeleteDirectoryDialog(final SubDirectory subDirectory) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle("Usun plik");
        builder.setMessage("Jesteś pewny? Tej akcji nie można cofnąć! Wszystie dane z tego " +
                "katalogu zostaną usunięte");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteDirectoryRequest(subDirectory);
            }
        });
        builder.setNegativeButton("Anuluj", null);
        builder.show();
    }

    private void deleteDirectoryRequest(SubDirectory subDirectory){
        new ApiRequest(this,
                new ApiData(ApiConstants.getDirectory()+"/"+subDirectory.getId(), null, EHttp
                        .DELETE, ERequest.DELETE_DIRECTORY), ResponseType.OBJECT, this).execute();
    }

    private void downloadFile(final File file) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Trwa pobieranie pliku...");
        progressDialog.show();

        final java.io.File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);;
        AsyncHttpClient httpClient = new AsyncHttpClient();
        String cookie = AppPreferences.getInstance().getCookies();
        if(!cookie.equals("")) {
            httpClient.addHeader("Cookie", cookie);
        }
        try {
            httpClient.get(ApiConstants.getServerAddress() + "directory/" + currentDirectory.getId() + "/file/" + file.getId(),
                    new AsyncHttpResponseHandler() {

                        @Override
                        public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                              Throwable arg3) {
                            progressDialog.dismiss();
                            showErrorMessage("Wystąpił błąd podczas pobierania pliku");
                        }

                        @Override
                        public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                            try {
                                FileOutputStream f = new FileOutputStream(new java.io.File(root, file.getName()));
                                f.write(arg2);
                                f.close();
                                Toast.makeText(getApplicationContext(), "Plik został zapisany w " + root.getPath(), Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
                                showErrorMessage("Wystąpił błąd podczas pobierania pliku");
                            } finally {
                                progressDialog.dismiss();
                            }

                        }
                    });
        } catch(Exception e) {
            progressDialog.dismiss();
            showErrorMessage("Wystąpił błąd podczas pobierania pliku");
        }
    }

    private void showErrorMessage(String errorMessage) {
        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSendAddFileRequest(String filePath) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Trwa wysyłanie pliku...");
        progressDialog.show();

        java.io.File file = new java.io.File(filePath);
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String mimeType = mimeTypeMap.getMimeTypeFromExtension(filePath.substring((filePath.lastIndexOf(".") + 1), filePath.length()));

        AsyncHttpClient httpClient = new AsyncHttpClient();
        String cookie = AppPreferences.getInstance().getCookies();
        if(!cookie.equals("")) {
            httpClient.addHeader("Cookie", cookie);
        }
        httpClient.addHeader("mimetype", mimeType);
        httpClient.addHeader("filename", file.getName());
        RequestParams params = new RequestParams();
        try {
            params.put("File", new FileInputStream(file));
            httpClient.post(ApiConstants.getServerAddress() + "directory/" + currentDirectory.getId() + "/upload",
                    params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                      Throwable arg3) {
                    Toast.makeText(getApplicationContext(), "Wystąpił błąd podczas przesyłania pliku", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                    Toast.makeText(getApplicationContext(), "Plik został przesłany", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                    getDirectoryFromApi(currentDirectory.getId());
                    setMainFragment();
                }
            });
        } catch(FileNotFoundException e) {
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Plik nie istnieje", Toast.LENGTH_LONG).show();;
        }
    }

    @Override
    public void onSendAddUserRequest(String name, boolean readOnly) {
        Map<String, String> params = GlobalUtils.getRequestParams(
                asList("username", "directoryId"),
                asList(name, currentDirectory.getId().toString()));
        new ApiRequest(this,
                new ApiData(ApiConstants.getGrantUserAccess(), params, EHttp
                        .POST, ERequest.GRANT_USER_ACCESS), ResponseType.OBJECT, this).execute();
    }
}
