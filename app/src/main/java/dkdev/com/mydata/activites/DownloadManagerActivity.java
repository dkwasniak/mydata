package dkdev.com.mydata.activites;


import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import dkdev.com.mydata.AppPreferences;
import dkdev.com.mydata.R;
import dkdev.com.mydata.base.BaseActivity;
import dkdev.com.mydata.base.BaseFragment;
import dkdev.com.mydata.base.FragmentType;

public class DownloadManagerActivity extends BaseActivity {
    private long enqueue;
    private DownloadManager dm;
    String uriString;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_view);
        String fileId = getIntent().getExtras().getString("fileId");
        String directoryId = getIntent().getExtras().getString("directoryId");
        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse("http://46.101.136.26/directory/"+directoryId+"/file/"+fileId));
        request.addRequestHeader("Cookie", AppPreferences.getInstance().getCookies());
        enqueue = dm.enqueue(request);

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c
                                .getInt(columnIndex)) {

                            uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                            System.out.print("");
                            VideoView videoView = (VideoView) findViewById(R.id.videoView);
                            videoView.setMediaController(new MediaController(DownloadManagerActivity.this));
                            videoView.setVideoURI(Uri.parse(uriString));
                            videoView.requestFocus();
                            videoView.start();
                        }
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    @Override
    public void onChangeFragment(FragmentType type) {

    }

    @Override
    public void onChangeFragment(BaseFragment fragment) {

    }
}