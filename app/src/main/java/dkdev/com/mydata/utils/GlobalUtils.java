package dkdev.com.mydata.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class GlobalUtils {

	public static Map<String, String> getRequestParams(List<String> keys, List<String> values) {
		Map<String, String> params = new HashMap<>();
		if(keys.isEmpty() || values.isEmpty() || keys.size()!= values.size()) {
			throw new IllegalStateException("keys size not equals values size");
		} else {
			for (int i = 0; i <keys.size() ; i++) {
				params.put(keys.get(i),values.get(i));
			}
		}
		return params;
	}

}
