package dkdev.com.mydata.base;

import android.content.Context;

import dkdev.com.mydata.api.models.Directory;
import roboguice.fragment.RoboFragment;


public abstract class BaseFragment extends RoboFragment {



	public interface BaseFragmentListener {
		void onChangeFragment(FragmentType type);
		void onChangeFragment(BaseFragment fragment);
	}

	public BaseFragmentListener baseFragmentListener;


	@Override
	public void onAttach(final Context context) {
		super.onAttach(context);
		baseFragmentListener = (BaseActivity) context;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		baseFragmentListener = null;
	}

	public abstract FragmentType getFragmentType();


	protected BaseFragmentListener getBaseFragmentListener() {
		return baseFragmentListener;
	}

	protected void setBaseFragmentListener(
			final BaseFragmentListener baseFragmentListener) {
		this.baseFragmentListener = baseFragmentListener;
	}

}

