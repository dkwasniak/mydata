package dkdev.com.mydata.base;

import android.content.res.Resources;

import dkdev.com.mydata.login.fragments.UserLoginFragment;
import dkdev.com.mydata.login.fragments.UserRegisterFragment;


/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class FragmentsFactory {

	public static BaseFragment getFragmentByType(FragmentType type) {
		switch(type) {
			case USER_LOGIN_FRAGMENT:
				return UserLoginFragment.newInstance();

			case USER_REGISTER_FRAGMENT:
				return UserRegisterFragment.newInstance();
			default:
				throw new Resources.NotFoundException("Unable to find fragment with "
						+ "delivered fragment type:" + type.toString());
		}
	}

}
