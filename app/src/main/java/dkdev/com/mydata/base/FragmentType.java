package dkdev.com.mydata.base;

/**
 * Created by Damian Kwasniak on 08.06.15.
 */
public enum FragmentType {
	// login
	USER_LOGIN_FRAGMENT,
	USER_REGISTER_FRAGMENT,
	MAIN_FRAGMENT,
	SHOW_VIDEO_FRAGMENT,
	SHOW_IMAGE_FRAGMENT
}
