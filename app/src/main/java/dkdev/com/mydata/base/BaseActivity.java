package dkdev.com.mydata.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import roboguice.activity.RoboActionBarActivity;


/**
 * Created by Damian Kwasniak on 17.06.15.
 */
public abstract class BaseActivity extends RoboActionBarActivity implements BaseFragment.BaseFragmentListener {


	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}




}