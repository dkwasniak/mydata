package dkdev.com.mydata.api;

/**
 * Created by Damian Kwasniak on 08.06.15.
 */
public class ApiConstants {

	private static final String SERVER_ADDRESS = "http://46.101.136.26/";

	private static final String USER_REGISTER = "user/register";

	private static final String USER_LOGIN = "user/login";

	private static final String USER_LOGOUT = "user/logout";

	private static final String GET_DIRECTORY = "directory/";

	private static String ADD_DIRECTORY = "directory/";

	private static String GRAND_USER_ACCESS = "directory/access/grant";


	public static String getServerAddress() { return SERVER_ADDRESS; }

	public static String getUserRegister() {
		return USER_REGISTER;
	}

	public static String getUserLogin() {
		return USER_LOGIN;
	}

	public static String getUserLogout() {
		return USER_LOGOUT;
	}

	public static String getDirectory() {
		return GET_DIRECTORY;
	}

	public static String getAddDirectory() {
		return ADD_DIRECTORY;
	}

	public static String getGrantUserAccess() {
		return GRAND_USER_ACCESS;
	}
}
