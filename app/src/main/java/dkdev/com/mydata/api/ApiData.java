package dkdev.com.mydata.api;


import java.util.HashMap;
import java.util.Map;

import dkdev.com.mydata.api.enumeration.EHttp;
import dkdev.com.mydata.api.enumeration.ERequest;

/**
 * Created by Damian Kwasniak on 30.05.15.
 */
public class ApiData {

	private String apiURL;

	private String content;

	private EHttp httpType;

	private ERequest request;

	private Map<String, String> params;


	public ApiData(String apiURL, Map<String, String> params, EHttp type, ERequest requestType) {
		this.apiURL = apiURL;
		this.httpType = type;
		this.params = params;
		this.request = requestType;
	}

	public String getApiURL() {
		return apiURL;
	}

	public void setApiURL(final String apiURL) {
		this.apiURL = apiURL;
	}

	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}

	public int getHttpType() {
		return httpType.ordinal();
	}

	public void setHttpType(final EHttp httpType) {
		this.httpType = httpType;
	}

	public ERequest getRequest() {
		return request;
	}

	public void setRequest(final ERequest request) {
		this.request = request;
	}

	public Map<String, String> getParams() {
		if(params == null) {
			return new HashMap<>();
		}
		return params;
	}
}
