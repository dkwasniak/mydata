package dkdev.com.mydata.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dkdev.com.mydata.api.models.Directory;
import dkdev.com.mydata.api.models.User;

/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class ApiParser {

	private static Gson gson = new Gson();

	public static User getUserFromResponse(String response) {
		gson = new Gson();
		User user = gson.fromJson(getJsonElementByName(response, "user"),User.class);

		return user;
	}


	public static Directory getDirectoryFromResponse(String responseMessage) {
		return gson.fromJson(responseMessage, Directory.class);
	}

	public static ErrorMessage getErrorMessageFromResponse(String responseMessage) {
		return gson.fromJson(responseMessage,ErrorMessage.class);
	}

	private static JsonElement getJsonElementByName(String response, String name) {
		JsonParser parser = new JsonParser();
		JsonObject element = (JsonObject)parser.parse(response);
		JsonElement responseWrapper = element.get(name);

		return responseWrapper;
	}
}
