package dkdev.com.mydata.api.enumeration;

public enum ResponseType {
    OBJECT,
    ARRAY
}
