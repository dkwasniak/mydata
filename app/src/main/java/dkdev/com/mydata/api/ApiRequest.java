package dkdev.com.mydata.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dkdev.com.mydata.AppPreferences;
import dkdev.com.mydata.MyDataApplication;
import dkdev.com.mydata.R;
import dkdev.com.mydata.api.enumeration.ResponseType;

/**
 * Created by Damian Kwasniak on 31.05.15.
 */
public class ApiRequest {

    public interface OnNetworkListener {

        void onRequestSuccess(ApiResponse response);

        void onRequestFailed(ApiError error);
    }

    private static final String TAG = ApiRequest.class.getSimpleName();


    private Context context;

    private WalletJsonObjectRequest request;

    private WalletJsonArrayRequest requestArray;

    private ApiData apiData;

    private ResponseType responseType;

    private OnNetworkListener listener;

    private ProgressDialog pDialog;

    private ApiError apiError;

    public ApiRequest(Context context, ApiData apiData, ResponseType responseType, OnNetworkListener listener) {
        this.context = context;
        this.apiData = apiData;
        this.responseType = responseType;
        this.listener = listener;
    }

    public void execute() {
        showProgressDialog();
        switch (responseType) {
            case OBJECT:
            {
                request = new WalletJsonObjectRequest(apiData.getHttpType(), ApiConstants.getServerAddress() +
                        apiData.getApiURL(), new JSONObject(apiData.getParams()), responseListener, responseErrorListener, apiData) {


                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        String cookie = AppPreferences.getInstance().getCookies();
                        if(!cookie.equals("")) {
                            headers.put("Cookie", AppPreferences.getInstance().getCookies());
                        }
                        return headers;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyDataApplication.getInstance().addToRequestQueue(request, "request");
                break;
            }
            case ARRAY:
            {
                requestArray = new WalletJsonArrayRequest(ApiConstants.getServerAddress() + apiData.getApiURL(),
                        responseListenerArray, responseErrorListener) {


                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        String cookie = AppPreferences.getInstance().getCookies();
                        if(!cookie.equals("")) {
                            headers.put("Cookie", AppPreferences.getInstance().getCookies());
                        }
                        return headers;
                    }
                };
                requestArray.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyDataApplication.getInstance().addToRequestQueue(requestArray, "request");
                break;
            }
        }
    }


    Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            hideProgressDialog();
            if (listener != null && response != null) {
                listener.onRequestSuccess(new ApiResponse(response.toString(), apiData.getRequest()));
            } else {
                listener.onRequestSuccess(new ApiResponse("", apiData.getRequest()));
            }
        }
    };

    Response.Listener<JSONArray> responseListenerArray = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            hideProgressDialog();

            if (listener != null && response != null) {
                listener.onRequestSuccess(new ApiResponse(response.toString(), apiData.getRequest()));
            } else {
                listener.onRequestSuccess(new ApiResponse("", apiData.getRequest()));
            }
        }
    };


    ErrorListener responseErrorListener = new ErrorListener() {
        @Override
        public void onErrorResponse(final VolleyError error) {

            Log.d(TAG, error.toString());
            NetworkResponse networkResponse = error.networkResponse;
            String body = "";
            if (networkResponse != null && networkResponse.data != null) {
                body = new String(networkResponse.data);
            }
            if (networkResponse == null) {
                if (apiError == null) {
                    apiError = new ApiError();
                }
                apiError.setRequest(apiData.getRequest());
                apiError.setDeveloperMessage(context.getString(R.string.network_connection_error));
            } else {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                apiError = new ApiError(body, networkResponse.statusCode, apiData
                        .getRequest());
            }
            hideProgressDialog();
            if (listener != null) {
                listener.onRequestFailed(apiError);
            }
        }
    };

    private void showProgressDialog() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog != null) {
            pDialog.hide();
        }
    }

}
