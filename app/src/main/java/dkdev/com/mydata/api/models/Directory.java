package dkdev.com.mydata.api.models;

import org.parceler.Parcel;
import org.parceler.Parcel.Serialization;
import org.parceler.ParcelConstructor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Parcel(Serialization.BEAN)
public class Directory {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("readOnly")
    @Expose
    private Boolean readOnly;
    @SerializedName("pathToDirectory")
    @Expose
    private List<PathToDirectory> pathToDirectory = new ArrayList<PathToDirectory>();
    @SerializedName("subDirectories")
    @Expose
    private List<SubDirectory> subDirectories = new ArrayList<SubDirectory>();

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    @SerializedName("files")
    @Expose
    private List<File> files = new ArrayList<File>();

    @ParcelConstructor
    public Directory(String name,int id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof Directory)
        {
            sameSame = this.id == ((Directory) object).id;
        }

        return sameSame;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The readOnly
     */
    public Boolean getReadOnly() {
        return readOnly;
    }

    /**
     *
     * @param readOnly
     * The readOnly
     */
    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     *
     * @return
     * The pathToDirectory
     */
    public List<PathToDirectory> getPathToDirectory() {
        return pathToDirectory;
    }

    /**
     *
     * @param pathToDirectory
     * The pathToDirectory
     */
    public void setPathToDirectory(List<PathToDirectory> pathToDirectory) {
        this.pathToDirectory = pathToDirectory;
    }

    public ArrayList<String> getPathToDirectoryAsList() {
        ArrayList<String> list = new ArrayList<>();
        for (PathToDirectory path: pathToDirectory) {
            list.add(path.getName());
        }
        return list;
    }

    /**
     *
     * @return
     * The subDirectories
     */
    public List<SubDirectory> getSubDirectories() {
        return subDirectories;
    }

    /**
     *
     * @param subDirectories
     * The subDirectories
     */
    public void setSubDirectories(List<SubDirectory> subDirectories) {
        this.subDirectories = subDirectories;
    }

}
