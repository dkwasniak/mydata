package dkdev.com.mydata.api;


import dkdev.com.mydata.api.enumeration.ERequest;

/**
 * Created by Damian Kwasniak on 31.05.15.
 */
public class ApiError {

	private int status;

	private int code;

	private ErrorMessage developerMessage;

	private ERequest request;

	public ApiError(String message, int code, ERequest request) {
		this.developerMessage =  ApiParser.getErrorMessageFromResponse(message);
		this.code = code;
		this.request = request;
	}

	public ApiError() {

	}

	public int getStatus() {
		return status;
	}

	public void setStatus(final int status) {
		this.status = status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(final int code) {
		this.code = code;
	}

	public String getErrorMessage() {
		if(developerMessage !=null) {
			return developerMessage.getMessage();
		}
		return "";
	}


	public ERequest getRequest() {
		return request;
	}

	public void setRequest(final ERequest request) {
		this.request = request;
	}

	public void setDeveloperMessage(String developerMessage) {
		if (this.developerMessage == null) {
			this.developerMessage = new ErrorMessage();
		}
		this.developerMessage.setMessage(developerMessage);
	}
}
