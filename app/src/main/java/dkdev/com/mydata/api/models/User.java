package dkdev.com.mydata.api.models;

import com.google.gson.annotations.Expose;

/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class User {

	@Expose
	private Integer id;

	@Expose
	private String phoneNumber;

	@Expose
	private String username;

	@Expose
	private String authenticationToken;

	@Expose
	private Object email;

	/**
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return The phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber The phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return The username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username The username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return The authenticationToken
	 */
	public String getAuthenticationToken() {
		return authenticationToken;
	}

	/**
	 * @param authenticationToken The authenticationToken
	 */
	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}


	/**
	 * @return The email
	 */
	public Object getEmail() {
		return email;
	}

	/**
	 * @param email The email
	 */
	public void setEmail(Object email) {
		this.email = email;
	}


}
