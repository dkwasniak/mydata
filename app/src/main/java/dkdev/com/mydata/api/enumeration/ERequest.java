package dkdev.com.mydata.api.enumeration;

/**
 * Created by Damian Kwasniak on 30.05.15.
 */
public enum ERequest {
	// login
	USER_LOGIN_REQUEST,
	USER_LOGOUT_REQUEST,
	USER_REGISTER_REQUEST,
	GET_DIRECTORY, ADD_DIRECTORY,
	GET_FILE,
	DELETE_FILE, DELETE_DIRECTORY, GRANT_USER_ACCESS;
}
