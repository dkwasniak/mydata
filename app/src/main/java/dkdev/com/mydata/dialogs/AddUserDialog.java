package dkdev.com.mydata.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import dkdev.com.mydata.R;

public class AddUserDialog extends DialogFragment {

    public interface AddUserDialogCallback {

        void onSendAddUserRequest(String name, boolean readOnly);

    }

    private AddUserDialogCallback callback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context ctx = getActivity();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.add_directory, null, false);
        final EditText newDirectoryName = (EditText) rootView.findViewById(R.id.directoryNameTextView);
        final CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.checkBox);
        final CheckBox privcheckBox = (CheckBox) rootView.findViewById(R.id.privateCheckBox);
        checkBox.setVisibility(View.GONE);
        privcheckBox.setVisibility(View.GONE);

        return new AlertDialog.Builder(ctx)
                .setTitle("Dodaj użytkownika do folderu")
                .setView(rootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String name = newDirectoryName.getText().toString();
                        if (TextUtils.isEmpty(name)) {
                            Toast.makeText(getActivity(), "Nazwa nie moze być pusta", Toast.LENGTH_LONG);
                            return;
                        } else {
                            if (callback != null) {
                                callback.onSendAddUserRequest(name,checkBox.isSelected());
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof AddUserDialogCallback) {
            this.callback = (AddUserDialogCallback) activity;
        }
    }

    @Override
    public void onDetach() {
        this.callback = null;
        super.onDetach();
    }
}

