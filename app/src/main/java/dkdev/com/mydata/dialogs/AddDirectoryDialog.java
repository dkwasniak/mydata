package dkdev.com.mydata.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import dkdev.com.mydata.R;

public class AddDirectoryDialog extends DialogFragment {

    public interface AddDirectoryDialogCallback {

        void onSendAddDirectoryRequest(String name, boolean readOnly, boolean priv);

    }

    private AddDirectoryDialogCallback callback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context ctx = getActivity();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.add_directory, null, false);
        final EditText newDirectoryName = (EditText) rootView.findViewById(R.id.directoryNameTextView);
        final CheckBox checkBox = (CheckBox) rootView.findViewById(R.id.checkBox);
        final CheckBox privatCheckBox = (CheckBox) rootView.findViewById(R.id.privateCheckBox);

        return new AlertDialog.Builder(ctx)
                .setTitle("Dodaj folder")
                .setView(rootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String name = newDirectoryName.getText().toString();
                        if (TextUtils.isEmpty(name)) {
                            Toast.makeText(getActivity(), "Nazwa nie moze być pusta", Toast.LENGTH_LONG);
                            return;
                        } else {
                            if (callback != null) {
                                boolean a = privatCheckBox.isChecked();
                                callback.onSendAddDirectoryRequest(name,checkBox.isChecked(),privatCheckBox.isChecked());
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof AddDirectoryDialogCallback) {
            this.callback = (AddDirectoryDialogCallback) activity;
        }
    }

    @Override
    public void onDetach() {
        this.callback = null;
        super.onDetach();
    }
}

