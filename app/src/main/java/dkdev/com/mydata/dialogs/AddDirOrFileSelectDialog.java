package dkdev.com.mydata.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Aisa on 2015-11-24.
 */
public class AddDirOrFileSelectDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final CharSequence[] options = { "Dodaj folder", "Dodaj plik", "Dodaj użytkownika", "Anuluj" };

        return new AlertDialog.Builder(getActivity())
                .setTitle("Dodaj nowy zasób")
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Dodaj folder")) {
                            new AddDirectoryDialog().show(getActivity().getSupportFragmentManager(), "");
                        } else if (options[item].equals("Dodaj plik")) {
                            new AddFileDialog().show(getActivity().getSupportFragmentManager(), "");
                        } else if (options[item].equals("Dodaj użytkownika")) {
                            new AddUserDialog().show(getActivity().getSupportFragmentManager(), "");
                        } else if (options[item].equals("Anuluj")) {
                            dialog.dismiss();
                        }
                    }
                }).create();
    }

}
