package dkdev.com.mydata.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.net.HttpURLConnection;
import java.net.URL;

import dkdev.com.mydata.AppPreferences;
import dkdev.com.mydata.R;
import dkdev.com.mydata.api.ApiConstants;
import dkdev.com.mydata.api.models.Directory;
import dkdev.com.mydata.api.models.File;
import dkdev.com.mydata.base.BaseFragment;
import dkdev.com.mydata.base.FragmentType;

import static dkdev.com.mydata.base.FragmentType.SHOW_IMAGE_FRAGMENT;

/**
 * Created by Aisa on 2015-11-24.
 */
public class ShowImageFragment extends BaseFragment {
    File file;
    Directory directory;
    private static final String DIRECTORY = "directory";
    private static final String FILE = "file";
    private ImageView imageView;

    public static ShowImageFragment newInstance(Directory directory, File file) {
        ShowImageFragment fragment = new ShowImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(DIRECTORY, Parcels.wrap(directory));
        args.putParcelable(FILE, Parcels.wrap(file));
        fragment.setArguments(args);
        return fragment;
    }

    public ShowImageFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        directory = Parcels.unwrap(getArguments().getParcelable(DIRECTORY));
        file = Parcels.unwrap(getArguments().getParcelable(FILE));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.image_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = (ImageView) view.findViewById(R.id.imageView);

        String url = ApiConstants.getServerAddress() + "directory/" + directory.getId() + "/file/" + file.getId();
        ImageDownloader imageDownloader = new ImageDownloader(url);
        imageDownloader.execute();
    }

    @Override
    public FragmentType getFragmentType() {
        return SHOW_IMAGE_FRAGMENT;
    }

    private class ImageDownloader extends AsyncTask {
        private String requestUrl;
        private Bitmap pic;
        private ProgressDialog progressDialog;

        private ImageDownloader(String requestUrl) {
            this.requestUrl = requestUrl;
        }

        @Override
        protected Object doInBackground(Object... objects) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Trwa ładowanie pliku...");
                    progressDialog.show();
                }
            });
            try {
                URL url = new URL(requestUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                String cookie = AppPreferences.getInstance().getCookies();
                if(!cookie.equals("")) {
                    conn.setRequestProperty("Cookie", cookie);
                }
                conn.setRequestMethod("GET");
                if (conn.getResponseCode() == 200) {
                    pic = BitmapFactory.decodeStream(conn.getInputStream());
                } else {
                    showErrorMessage("Wystąpił błąd - status odpowiedzi" + conn.getResponseCode());
                }
            } catch (final Exception ex) {
                showErrorMessage(ex.getMessage().toString());
            }
            return null;
        }

        private void showErrorMessage(final String errorMessage) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        protected void onPostExecute(Object o) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                }
            });
            imageView.setImageBitmap(pic);
        }
    }
}
