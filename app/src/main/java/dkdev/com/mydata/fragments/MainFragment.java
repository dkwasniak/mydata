package dkdev.com.mydata.fragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import dkdev.com.mydata.R;
import dkdev.com.mydata.adapters.DirectoryAdapter;
import dkdev.com.mydata.adapters.PathAdapter;
import dkdev.com.mydata.api.models.Directory;
import dkdev.com.mydata.api.models.File;
import dkdev.com.mydata.api.models.SubDirectory;
import dkdev.com.mydata.base.BaseFragment;
import dkdev.com.mydata.base.FragmentType;
import roboguice.inject.InjectView;

import static dkdev.com.mydata.base.FragmentType.MAIN_FRAGMENT;

public class MainFragment extends BaseFragment implements PathAdapter.PathAdapterCallback {

    public interface MainFragmentCallback {

        void onDirectoryClicked(int id);

        void onFileClicked(File file);

        void onFileLongClick(File file);

        void onDirectoryLongClick(SubDirectory subDirectory);
    }

    private static final String DIRECTORY = "directory";
    private Directory directory;

    @InjectView(R.id.catalogNameTextView)
    private TextView catalogNameTextView;
    @InjectView(R.id.pathRecycleView)
    private RecyclerView pathRecycleView;
    @InjectView(R.id.directoryRecycleView)
    private RecyclerView directoryRecycleView;

    private RecyclerView.Adapter pathAdapter;
    private RecyclerView.Adapter directoryAdapter;

    private MainFragmentCallback callback;


    public static MainFragment newInstance(Directory directory) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putParcelable(DIRECTORY, Parcels.wrap(directory));
        fragment.setArguments(args);
        return fragment;
    }

    public MainFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        directory = Parcels.unwrap(getArguments().getParcelable(DIRECTORY));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pathRecycleView.setHasFixedSize(true);
        pathRecycleView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        pathAdapter = new PathAdapter();
        pathRecycleView.setAdapter(pathAdapter);

        directoryRecycleView.setHasFixedSize(true);
        directoryRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        directoryAdapter = new DirectoryAdapter();
        directoryRecycleView.setAdapter(directoryAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainFragmentCallback) {
            callback = (MainFragmentCallback) context;
        }
    }

    @Override
    public void onDetach() {
        callback = null;
        super.onDetach();
    }

    @Override
    public FragmentType getFragmentType() {
        return MAIN_FRAGMENT;
    }

    @Override
    public void onPathItemClicked(int id) {
        callback.onDirectoryClicked(id);
    }

    public void reloadAdapters(Directory directory) {
        this.directory = directory;
        catalogNameTextView.setText(directory.getName());
        pathAdapter = new PathAdapter(directory,this);
        directoryAdapter = new DirectoryAdapter(directory, new DirectoryAdapter.DirectoryAdapterCallback() {
            @Override
            public void onItemClick(Directory directory) {

            }

            @Override
            public void onFileClicked(File file) {
                if(callback != null) {
                    callback.onFileClicked(file);
                }
            }

            @Override
            public void onSubDirectoryClicked(SubDirectory subDirectory) {
                if(callback != null){
                    callback.onDirectoryClicked(subDirectory.getId());
                }
            }

            @Override
            public void onFileLongClick(File file) {
                if(callback != null) {
                    callback.onFileLongClick(file);
                }
            }

            @Override
            public void onSubDirectoryLongClick(SubDirectory subDirectory) {
                if(callback != null){
                    callback.onDirectoryLongClick(subDirectory);
                }
            }
        });
        pathRecycleView.setAdapter(pathAdapter);
        directoryRecycleView.setAdapter(directoryAdapter);
        directoryAdapter.notifyDataSetChanged();
        pathAdapter.notifyDataSetChanged();
    }
}
