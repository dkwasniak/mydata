package dkdev.com.mydata.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.Serializable;

import dkdev.com.mydata.AppPreferences;
import dkdev.com.mydata.R;
import dkdev.com.mydata.activites.MainActivity;
import dkdev.com.mydata.base.BaseActivity;
import dkdev.com.mydata.base.BaseFragment;
import dkdev.com.mydata.base.FragmentType;
import dkdev.com.mydata.base.FragmentsFactory;


public class LoginActivity extends BaseActivity {

	private FragmentType currentFragmentType = FragmentType.USER_LOGIN_FRAGMENT;

	private final static String LOGIN_FRAGMENT = "login_fragment";

	private int backPressCounter = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		setContentView(R.layout.activity_login);
		checkIfUserLogged();

		initUI();
	}

	private void checkIfUserLogged() {
		if(!AppPreferences.getInstance().getCookies().equals("")) {
			startActivity(new Intent(this, MainActivity.class));
		}
	}


	@Override
	public void onBackPressed() {
		if (getFragmentManager().getBackStackEntryCount() > 0
				&& getCurrentFragmentType() != FragmentType.USER_LOGIN_FRAGMENT) {
			getFragmentManager().popBackStack();
		} else {
			backPressCounter++;
			if(backPressCounter == 2) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						backPressCounter=0;
					}
				},1000);
				Toast.makeText(this,R.string.press_again_to_exit,Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void initUI() {
		setMainFragment();
	}


	public void setMainFragment() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.main_container, FragmentsFactory.getFragmentByType(currentFragmentType),
				LOGIN_FRAGMENT)
				.addToBackStack(LOGIN_FRAGMENT).commit();
	}

	@Override
	public void onChangeFragment(FragmentType type) {
		currentFragmentType = type;
		setMainFragment();
	}

	@Override
	public void onChangeFragment(BaseFragment fragment) {

	}

	private FragmentType getCurrentFragmentType() {
		return ((BaseFragment)(
				getSupportFragmentManager().findFragmentByTag(LOGIN_FRAGMENT))).getFragmentType();
	}
}
